"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var electron_1 = require("electron");
var PanelManager = /** @class */ (function () {
    function PanelManager(config) {
        this.config = config;
        this.panelStructure = [];
        this.listen();
    }
    PanelManager.prototype.listen = function () {
        var _this = this;
        electron_1.ipcMain.on("panel structure", function (event, request) {
            _this.loadPanelConfig().then(function (data) {
                _this.panelStructure = data;
                event.sender.send("panel structure", _this.panelStructure);
            }).catch(function (error) {
                console.error("Panel structure could not be loaded:", error);
            });
        });
    };
    /**
     * Loads the panel structure from the json file
     */
    PanelManager.prototype.loadPanelConfig = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            fs.readFile(_this.config.configDir + "/panels.json", "utf8", function (err, data) {
                console.log(_this.config.configDir + "/panels.json");
                try {
                    var parsedData = JSON.parse(data);
                    resolve(parsedData);
                }
                catch (error) {
                    reject(error);
                }
            });
        });
    };
    return PanelManager;
}());
exports.PanelManager = PanelManager;
//# sourceMappingURL=PanelManager.js.map