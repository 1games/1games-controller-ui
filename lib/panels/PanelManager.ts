import * as fs from "fs";
import { Config } from "../config/Config";
import { ipcMain } from "electron";

export class PanelManager{


    private panelStructure: Array<Container> = [];


    constructor(private config: Config){
        this.listen();
    }


    private listen(){
        ipcMain.on("panel structure", (event, request) => {

            this.loadPanelConfig().then(data => {

                this.panelStructure = data;
                
                event.sender.send("panel structure", this.panelStructure);
                
            }).catch(error => {
                console.error("Panel structure could not be loaded:", error);
            })
    
        });
    }


    /**
     * Loads the panel structure from the json file
     */
    public loadPanelConfig(): Promise<Array<Container>>{
        return new Promise<Array<Container>>((resolve, reject) => {
            fs.readFile(this.config.configDir + "/panels.json", "utf8", (err, data) => {
                console.log(this.config.configDir + "/panels.json")
                try{
                    var parsedData = JSON.parse(data);
                    resolve(parsedData);
                }catch(error){
                    reject(error)
                }
                
            });
        });
    }

}

export interface Container{
  type: any;
  data: any;
  execute: Execute;
}

export interface Execute{
    type: string,

    id?: string, // Replace with enum
    // TODO: Marketplace application id
}