import * as fs from "fs";
import { ipcMain, BrowserWindow } from "electron";
import { ROOT_PATH } from "./../../main";

export class Config{


    public configDir: string = ROOT_PATH + "/config";
    public dataPath: string = ROOT_PATH + "/data";
    


    constructor(public win: BrowserWindow, public path: string){
        this.listen();
    }

    private listen(){
        ipcMain.on("config", (event, request) => {
            this.win.webContents.send("config", this[request]);
        });

        // TODO: Move this into a seperate panel service
        
    }



    public loadConfig(){
        fs.readFile(this.path, "utf8", (err, data) => {
            if(err){
                throw err;
            }

            var parsedData;
            try{
                parsedData = JSON.parse(data);
            }catch(jsonErr){
                throw new Error("Invalid config syntax: " + JSON.stringify(jsonErr));
            }

            for(let key of Object.keys(parsedData)){
                this[key] = parsedData[key];
            }

        });
    }

    public get<T = any>(key: string): T{
        return <T>this[key];
    }
}