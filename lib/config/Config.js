"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var electron_1 = require("electron");
var main_1 = require("./../../main");
var Config = /** @class */ (function () {
    function Config(win, path) {
        this.win = win;
        this.path = path;
        this.configDir = main_1.ROOT_PATH + "/config";
        this.dataPath = main_1.ROOT_PATH + "/data";
        this.listen();
    }
    Config.prototype.listen = function () {
        var _this = this;
        electron_1.ipcMain.on("config", function (event, request) {
            _this.win.webContents.send("config", _this[request]);
        });
        // TODO: Move this into a seperate panel service
    };
    Config.prototype.loadConfig = function () {
        var _this = this;
        fs.readFile(this.path, "utf8", function (err, data) {
            if (err) {
                throw err;
            }
            var parsedData;
            try {
                parsedData = JSON.parse(data);
            }
            catch (jsonErr) {
                throw new Error("Invalid config syntax: " + JSON.stringify(jsonErr));
            }
            for (var _i = 0, _a = Object.keys(parsedData); _i < _a.length; _i++) {
                var key = _a[_i];
                _this[key] = parsedData[key];
            }
        });
    };
    Config.prototype.get = function (key) {
        return this[key];
    };
    return Config;
}());
exports.Config = Config;
//# sourceMappingURL=Config.js.map