

export class DataCache{


    private _cache = {};
    private cachedObjects: number = 0;

    constructor(private maxCacheLength: number = 200){}

    public cache(key: string, data: any){
        if(this.cachedObjects >= this.maxCacheLength){
            this.releaseRandom()
        }
        this._cache[key] = data;
        this.cachedObjects++;
    }

    public get<T>(key: string): T{
        return <T> this._cache[key];
    }

    public release(key: string){
        delete this._cache[key];
        this.cachedObjects--;
    }

    public releaseRandom(){
        this.release(Object.keys(this._cache)[Math.random() * (this.maxCacheLength - 1)]);
    }

    public contains(key: string): boolean{
        return this._cache[key];
    }


}