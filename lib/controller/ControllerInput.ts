
import { ipcMain, BrowserWindow } from "electron";
import * as gamepad from "gamepad";

export class ControllerInputHandler{

    constructor(private win: BrowserWindow){

        gamepad.init();

        setInterval(gamepad.processEvents, 50);

        setInterval(gamepad.detectDevices, 500);

        this.setupListeners();

    }

 
    setupListeners(){

        gamepad.on("up", (id, num) => {

            const event = new ControllerInputEvent();

            // event.type = InputType.UP;
            event.controller = id;
            event.key = this.getInputKeyFromNumber(num);
            event.value = 0;
            
            this.sendClient(event);
        })

        gamepad.on("down", (id, num) => {

            const event = new ControllerInputEvent();

            // event.type = InputType.UP;
            event.controller = id;
            event.key = this.getInputKeyFromNumber(num);
            event.value = 1;
        


            this.sendClient(event);
        })

        gamepad.on("move", (id, num, value) => {

            const event = new ControllerInputEvent();

            // event.type = InputType.UP;
            event.controller = id;
            event.key = this.getAxisInputKeyFromNumber(num);
            event.value = value;


            this.sendClient(event);
        })
    }

    


    private sendClient(controllerInput: ControllerInputEvent){
        this.win.webContents.send("controller input", controllerInput);
    }

    public getAxisInputKeyFromNumber(num: number): InputKey{
        switch(num){
            case 0: return InputKey.AXIS1_X
            case 1: return InputKey.AXIS1_Y

            case 2: return InputKey.AXIS2_X
            case 3: return InputKey.AXIS2_Y

            case 4: return InputKey.TRIGGER_LEFT
            case 5: return InputKey.TRIGGER_RIGHT
        }
    }

    public getInputKeyFromNumber(num: number): InputKey{
        switch(num){
            case 0: return InputKey.UP
            case 1: return InputKey.DOWN
            case 2: return InputKey.LEFT
            case 3: return InputKey.RIGHT

            case 4: return InputKey.START
            case 5: return InputKey.BACK

            case 6: return InputKey.AXIS1_PUSH
            case 7: return InputKey.AXIS2_PUSH

            case 8: return InputKey.BUMPER_LEFT
            case 9: return InputKey.BUMPER_RIGHT

            case 10: return InputKey.BUTTON_A
            case 11: return InputKey.BUTTON_B
            case 12: return InputKey.BUTTON_X
            case 13: return InputKey.BUTTON_Y

            case 14: return InputKey.HOME
        }
    }
    

}


export class ControllerInputEvent{
    constructor(
        public controller: number = 0,
        public key: InputKey = null,
        public value: number = 0){

    }
}

export enum InputKey{
    AXIS1_X = "AXIS1_X",
    AXIS1_Y = "AXIS1_Y",

    AXIS2_X = "AXIS2_X",
    AXIS2_Y = "AXIS2_Y",

    AXIS1_PUSH = "AXIS1_PUSH",
    AXIS2_PUSH = "AXIS2_PUSH",

    LEFT = "LEFT",
    RIGHT = "RIGHT",
    UP = "UP",
    DOWN = "DOWN",

    BUTTON_A = "BUTTON_A",
    BUTTON_B = "BUTTON_B",
    BUTTON_X = "BUTTON_X",
    BUTTON_Y = "BUTTON_Y",

    BACK = "BACK",
    START = "START",

    BUMPER_RIGHT = "BUMPER_RIGHT",
    BUMPER_LEFT = "BUMPER_LEFT",

    TRIGGER_RIGHT = "TRIGGER_RIGHT",
    TRIGGER_LEFT = "TRIGGER_LEFT",

    HOME = "HOME",
}