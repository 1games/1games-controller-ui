"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var gamepad = require("gamepad");
var ControllerInputHandler = /** @class */ (function () {
    function ControllerInputHandler(win) {
        this.win = win;
        gamepad.init();
        setInterval(gamepad.processEvents, 50);
        setInterval(gamepad.detectDevices, 500);
        this.setupListeners();
    }
    ControllerInputHandler.prototype.setupListeners = function () {
        var _this = this;
        gamepad.on("up", function (id, num) {
            var event = new ControllerInputEvent();
            // event.type = InputType.UP;
            event.controller = id;
            event.key = _this.getInputKeyFromNumber(num);
            event.value = 0;
            _this.sendClient(event);
        });
        gamepad.on("down", function (id, num) {
            var event = new ControllerInputEvent();
            // event.type = InputType.UP;
            event.controller = id;
            event.key = _this.getInputKeyFromNumber(num);
            event.value = 1;
            _this.sendClient(event);
        });
        gamepad.on("move", function (id, num, value) {
            var event = new ControllerInputEvent();
            // event.type = InputType.UP;
            event.controller = id;
            event.key = _this.getAxisInputKeyFromNumber(num);
            event.value = value;
            _this.sendClient(event);
        });
    };
    ControllerInputHandler.prototype.sendClient = function (controllerInput) {
        this.win.webContents.send("controller input", controllerInput);
    };
    ControllerInputHandler.prototype.getAxisInputKeyFromNumber = function (num) {
        switch (num) {
            case 0: return InputKey.AXIS1_X;
            case 1: return InputKey.AXIS1_Y;
            case 2: return InputKey.AXIS2_X;
            case 3: return InputKey.AXIS2_Y;
            case 4: return InputKey.TRIGGER_LEFT;
            case 5: return InputKey.TRIGGER_RIGHT;
        }
    };
    ControllerInputHandler.prototype.getInputKeyFromNumber = function (num) {
        switch (num) {
            case 0: return InputKey.UP;
            case 1: return InputKey.DOWN;
            case 2: return InputKey.LEFT;
            case 3: return InputKey.RIGHT;
            case 4: return InputKey.START;
            case 5: return InputKey.BACK;
            case 6: return InputKey.AXIS1_PUSH;
            case 7: return InputKey.AXIS2_PUSH;
            case 8: return InputKey.BUMPER_LEFT;
            case 9: return InputKey.BUMPER_RIGHT;
            case 10: return InputKey.BUTTON_A;
            case 11: return InputKey.BUTTON_B;
            case 12: return InputKey.BUTTON_X;
            case 13: return InputKey.BUTTON_Y;
            case 14: return InputKey.HOME;
        }
    };
    return ControllerInputHandler;
}());
exports.ControllerInputHandler = ControllerInputHandler;
var ControllerInputEvent = /** @class */ (function () {
    function ControllerInputEvent(controller, key, value) {
        if (controller === void 0) { controller = 0; }
        if (key === void 0) { key = null; }
        if (value === void 0) { value = 0; }
        this.controller = controller;
        this.key = key;
        this.value = value;
    }
    return ControllerInputEvent;
}());
exports.ControllerInputEvent = ControllerInputEvent;
var InputKey;
(function (InputKey) {
    InputKey["AXIS1_X"] = "AXIS1_X";
    InputKey["AXIS1_Y"] = "AXIS1_Y";
    InputKey["AXIS2_X"] = "AXIS2_X";
    InputKey["AXIS2_Y"] = "AXIS2_Y";
    InputKey["AXIS1_PUSH"] = "AXIS1_PUSH";
    InputKey["AXIS2_PUSH"] = "AXIS2_PUSH";
    InputKey["LEFT"] = "LEFT";
    InputKey["RIGHT"] = "RIGHT";
    InputKey["UP"] = "UP";
    InputKey["DOWN"] = "DOWN";
    InputKey["BUTTON_A"] = "BUTTON_A";
    InputKey["BUTTON_B"] = "BUTTON_B";
    InputKey["BUTTON_X"] = "BUTTON_X";
    InputKey["BUTTON_Y"] = "BUTTON_Y";
    InputKey["BACK"] = "BACK";
    InputKey["START"] = "START";
    InputKey["BUMPER_RIGHT"] = "BUMPER_RIGHT";
    InputKey["BUMPER_LEFT"] = "BUMPER_LEFT";
    InputKey["TRIGGER_RIGHT"] = "TRIGGER_RIGHT";
    InputKey["TRIGGER_LEFT"] = "TRIGGER_LEFT";
    InputKey["HOME"] = "HOME";
})(InputKey = exports.InputKey || (exports.InputKey = {}));
//# sourceMappingURL=ControllerInput.js.map