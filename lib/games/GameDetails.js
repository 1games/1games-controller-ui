"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var DataCache_1 = require("./../DataCache");
var Utils_1 = require("./../utils/Utils");
var fs = require("fs");
var GameDetails = /** @class */ (function () {
    // TODO: Replace browser window with custom window manager
    function GameDetails(win, config) {
        this.win = win;
        this.config = config;
        // TODO: Inject main config object
        this.service = new GameDetailsService(this.config);
        this.setupListener();
    }
    GameDetails.prototype.setupListener = function () {
        var _this = this;
        electron_1.ipcMain.on("game basics", function (event, request) {
            var games = _this.service.getGameList();
        });
        electron_1.ipcMain.on("game details", function (event, request) {
            console.log(JSON.stringify(request));
            _this.service.getGameData(request.id).then(function (data) {
                _this.win.webContents.send("game details", data);
            }).catch(function (reason) {
                _this.win.webContents.send("error", reason);
            });
        });
        electron_1.ipcMain.on("game start", function (event, request) {
            var exec = require('child_process').exec;
            _this.service.getGameData(request.id).then(function (data) {
                console.log("start \"" + data.executable + "\"");
                exec("\"" + data.executable + "\"", function (error, stdout, stderr) {
                    console.log(error, stdout, stderr);
                });
            }).catch(function (reason) {
            });
        });
    };
    return GameDetails;
}());
exports.GameDetails = GameDetails;
var GameDetailsService = /** @class */ (function () {
    function GameDetailsService(config) {
        var _this = this;
        this.config = config;
        this.PUBLIC_KEY = "";
        this.games = {};
        this.cache = new DataCache_1.DataCache();
        // Loads all ids and game names from the game index file (<configDir>/games/index.json)
        this.loadData("index").then(function (gameIDs) {
            console.log("Loading games...");
            _this.games = gameIDs;
            var _loop_1 = function (gameID) {
                _this.loadData(gameIDs[gameID]).then(function (game) {
                    console.log("Loaded game " + game.name);
                    _this.games[gameIDs[gameID]] = game;
                });
            };
            // Iterate through every game id
            for (var _i = 0, _a = Object.keys(gameIDs); _i < _a.length; _i++) {
                var gameID = _a[_i];
                _loop_1(gameID);
            }
        });
    }
    /**
     * Loads more detailed informations about a game like images or user data
     * @param id The game id to get informations from
     */
    GameDetailsService.prototype.getGameData = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                // I don't know why it is null sometimes...
                if (!id) {
                    // TODO: well... add some errors here 
                }
                if (this.cache.contains(id)) {
                    return [2 /*return*/, this.cache.get(id)];
                }
                else {
                    return [2 /*return*/, this.loadData(id)];
                }
                return [2 /*return*/];
            });
        });
    };
    /**
     * Loads basic informations about all games like panel images, name and id
     */
    GameDetailsService.prototype.getGameList = function () {
        return this.games;
    };
    /**
     * Loads informations for games
     * @param key Either "games" for loading all game ids or a game id to load
     */
    GameDetailsService.prototype.loadData = function (key) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            fs.readFile(_this.config.get("configDir") + "/games/" + key + ".json", "utf8", function (err, raw) {
                // This is a fixed key for loading the game list! ISSUE:
                // TODO: Make a seperate method for loading the game list
                if (key == "index") {
                    resolve(JSON.parse(raw));
                }
                else {
                    // Returns an error if gamedata file could not be found;
                    if (err) {
                        reject(new GameDataNotFoundError(key));
                        return;
                    }
                    var gameData = new GameDetailsData();
                    // Assigns every property from the config file to a js object
                    Utils_1.Utils.assignAllValues(gameData, JSON.parse(raw));
                    _this.cache.cache(key, gameData);
                    resolve(gameData);
                }
            });
        });
    };
    return GameDetailsService;
}());
exports.GameDetailsService = GameDetailsService;
var BasicGameDetails = /** @class */ (function () {
    function BasicGameDetails() {
    }
    return BasicGameDetails;
}());
exports.BasicGameDetails = BasicGameDetails;
var GameDetailsData = /** @class */ (function (_super) {
    __extends(GameDetailsData, _super);
    function GameDetailsData() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return GameDetailsData;
}(BasicGameDetails));
exports.GameDetailsData = GameDetailsData;
var GameDataNotFoundError = /** @class */ (function (_super) {
    __extends(GameDataNotFoundError, _super);
    function GameDataNotFoundError(key) {
        var _this = _super.call(this, "GameData file for key \"" + key + "\" could not be found. ") || this;
        _this.key = key;
        return _this;
    }
    return GameDataNotFoundError;
}(Error));
exports.GameDataNotFoundError = GameDataNotFoundError;
//# sourceMappingURL=GameDetails.js.map