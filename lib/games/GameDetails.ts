import { ipcMain, BrowserWindow } from "electron";
import { DataCache } from "./../DataCache";
import { Utils } from "./../utils/Utils";
import { Config } from "../config/Config";

import * as fs from "fs";

export class GameDetails{

    // TODO: Inject main config object
    private service: GameDetailsService = new GameDetailsService(this.config);

    // TODO: Replace browser window with custom window manager
    constructor(public win: BrowserWindow, public config: Config){
        this.setupListener();
    }

    private setupListener(){

        ipcMain.on("game basics", (event, request) => {

            var games = this.service.getGameList();

        });

        ipcMain.on("game details", (event, request) => {

            console.log(JSON.stringify(request));

            this.service.getGameData(request.id).then(data => {
                this.win.webContents.send("game details", data);
            }).catch(reason => {
                this.win.webContents.send("error", reason);
            });




        });

        ipcMain.on("game start", (event, request) => {
            
            var exec = require('child_process').exec;

            this.service.getGameData(request.id).then(data => {
                console.log(`start "${data.executable}"`)
                exec(`"${data.executable}"`, (error, stdout, stderr) => {

                    console.log(error, stdout, stderr);

                })
            }).catch(reason => {

            });



        })
    }
}


export class GameDetailsService{
    public PUBLIC_KEY: string = "";
    private games = {};
    private cache = new DataCache();

    constructor(private config: Config){
 
        // Loads all ids and game names from the game index file (<configDir>/games/index.json)
        this.loadData("index").then(gameIDs => {
            console.log("Loading games...");

            this.games = gameIDs;
 
            // Iterate through every game id
            for(let gameID of Object.keys(gameIDs)){

                this.loadData(gameIDs[gameID]).then(game => {
 
                    console.log(`Loaded game ${game.name}`);

                    this.games[gameIDs[gameID]] = <BasicGameDetails>game;

                });

            }
        });
    }


    /**
     * Loads more detailed informations about a game like images or user data
     * @param id The game id to get informations from
     */
    public async getGameData(id: string): Promise<GameDetailsData>{

        // I don't know why it is null sometimes...
        if(!id){
            // TODO: well... add some errors here 
        }

        if(this.cache.contains(id)){
            return this.cache.get<GameDetailsData>(id);
        }else{
            return this.loadData(id);
        }
        

    }

    /**
     * Loads basic informations about all games like panel images, name and id
     */
    public getGameList(): any{
        return this.games 
    }



    /**
     * Loads informations for games
     * @param key Either "games" for loading all game ids or a game id to load
     */
    public loadData(key: string): Promise<GameDetailsData | any>{
        return new Promise((resolve, reject) => {
            fs.readFile(this.config.get<string>("configDir") + "/games/" + key + ".json", "utf8", (err, raw) => {

                // This is a fixed key for loading the game list! ISSUE:
                // TODO: Make a seperate method for loading the game list
                if(key == "index"){
                    resolve(JSON.parse(raw))
                }else{

                    // Returns an error if gamedata file could not be found;
                    if(err){
                        reject(new GameDataNotFoundError(key));
                        return;
                    }

                    var gameData = new GameDetailsData();

                    // Assigns every property from the config file to a js object
                    Utils.assignAllValues(gameData, JSON.parse(raw));

                    this.cache.cache(key, gameData);

                    resolve(gameData);
                }
            })
        }); 
    }

    

}


export class BasicGameDetails{
    name: string;

    // Just file names, not the data or a path

    imgSquare?: string;
    imgWide?: string;
    imgDefault?: string;
    imgBackground?: string;

    executable?: string;
}

export class GameDetailsData extends BasicGameDetails{
    

    lastPlayed?: Date;

    screenshots?: Array<string>;

}


export class GameDataNotFoundError extends Error{

    constructor(public key: string){
        super("GameData file for key \""+ key + "\" could not be found. ");
    }
}