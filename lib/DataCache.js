"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DataCache = /** @class */ (function () {
    function DataCache(maxCacheLength) {
        if (maxCacheLength === void 0) { maxCacheLength = 200; }
        this.maxCacheLength = maxCacheLength;
        this._cache = {};
        this.cachedObjects = 0;
    }
    DataCache.prototype.cache = function (key, data) {
        if (this.cachedObjects >= this.maxCacheLength) {
            this.releaseRandom();
        }
        this._cache[key] = data;
        this.cachedObjects++;
    };
    DataCache.prototype.get = function (key) {
        return this._cache[key];
    };
    DataCache.prototype.release = function (key) {
        delete this._cache[key];
        this.cachedObjects--;
    };
    DataCache.prototype.releaseRandom = function () {
        this.release(Object.keys(this._cache)[Math.random() * (this.maxCacheLength - 1)]);
    };
    DataCache.prototype.contains = function (key) {
        return this._cache[key];
    };
    return DataCache;
}());
exports.DataCache = DataCache;
//# sourceMappingURL=DataCache.js.map