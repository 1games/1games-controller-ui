"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utils;
(function (Utils) {
    function assignAllValues(object, toAssign) {
        for (var _i = 0, _a = Object.keys(toAssign); _i < _a.length; _i++) {
            var key = _a[_i];
            object[key] = toAssign[key];
        }
    }
    Utils.assignAllValues = assignAllValues;
})(Utils = exports.Utils || (exports.Utils = {}));
//# sourceMappingURL=Utils.js.map