
export namespace Utils{

    export function assignAllValues(object: any, toAssign: any){
        for(let key of Object.keys(toAssign)){
            object[key] = toAssign[key];
        }
    }
}