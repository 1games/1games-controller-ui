import { Component, Input, ElementRef, forwardRef } from "@angular/core";
import { ControllerContainerComponent } from "../../app/components/controller-container/controller-container.component";
import { Container } from "../../app/providers/panel.service";
import { ControllerInputEvent, ControllerInputService, InputKey } from "../../app/providers/controller-input.service";
import { Keyboard } from "../../app/keyboard/keyboard.service";


@Component({
    selector: "c-input",
    styleUrls: ["./input.component.less"],
    templateUrl: "./input.component.html",
    providers: [{provide: ControllerContainerComponent, useExisting: forwardRef(() => InputComponent) }]
})
export class InputComponent extends ControllerContainerComponent{

    private _type: string = "text"
    public get type(): string{return this._type};
    @Input() public set type(value){
        this._type = value;
        if(this.type == "text" || this.type == "number") this.value = ""
        else if(this.type == "checkbox") this.value = false;
    };
    @Input() public value: any;
    @Input() public label: any;

    constructor(eRef: ElementRef, controller: ControllerInputService, private keyboard: Keyboard){
        super(eRef, controller);
    }

    
    public init(cont: Container){
        
    }

    
    onInput(event: ControllerInputEvent){
        if(event.key == InputKey.BUTTON_A){

            if(this.type == "checkbox"){
                this.value = !this.value;
            }

            if(this.type == "text" || this.type == "number"){
                this.keyboard.get({
                    initialValue: this.value
                }).then(value => {
                    this.value = value;
                }).catch(error => {

                })
            }
        }
    };
}