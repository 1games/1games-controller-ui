import { Component, Input, ElementRef, forwardRef, EventEmitter, Output, HostBinding } from "@angular/core";
import { ControllerContainerComponent } from "../../app/components/controller-container/controller-container.component";
import { Container } from "../../app/providers/panel.service";
import { ControllerInputEvent, Inputable, ControllerInputService, InputKey } from "../../app/providers/controller-input.service";


@Component({
    selector: "c-button",
    styleUrls: ["./button.component.less"],
    template: `
        <ng-content></ng-content>`,
    providers: [{provide: ControllerContainerComponent, useExisting: forwardRef(() => ButtonComponent) }]
})
export class ButtonComponent extends ControllerContainerComponent implements Inputable{

    @Input() public src: string;  
    @Output() public input = new EventEmitter<ControllerInputEvent>(true);
    
    constructor(eRef: ElementRef, controller: ControllerInputService){
        super(eRef, controller)
    }

    
    public init(cont: Container){

    }

    onInput(event: ControllerInputEvent){
        if(this.selected){
            this.input.emit(event);
        }
    }
}