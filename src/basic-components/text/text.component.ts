import { Component, Input, ElementRef } from "@angular/core";
import { ControllerContainerComponent } from "../../app/components/controller-container/controller-container.component";
import { Container } from "../../app/providers/panel.service";
import { ControllerInputEvent, ControllerInputService } from "../../app/providers/controller-input.service";


@Component({
    selector: "c-text",
    template: `
        <span>{{this.text}}</span>`
})
export class TextComponent extends ControllerContainerComponent{

    @Input() public text: string = "";
    
    constructor(eRef: ElementRef, controller: ControllerInputService){
        super(eRef, controller)
    }

    
    public init(cont: Container){
        this.text = cont.data.text;
    }

    onInput(event: ControllerInputEvent){};
}