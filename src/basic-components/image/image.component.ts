import { Component, Input, ElementRef, forwardRef } from "@angular/core";
import { ControllerContainerComponent } from "../../app/components/controller-container/controller-container.component";
import { Container } from "../../app/providers/panel.service";
import { ControllerInputEvent, ControllerInputService } from "../../app/providers/controller-input.service";


@Component({
    selector: "c-img",
    styleUrls: ["./image.component.less"],
    template: `
        <img [src]="this.src" [alt]="this.alt" [style.height]="this.height" [style.width]="this.width">`,
    providers: [{provide: ControllerContainerComponent, useExisting: forwardRef(() => ImageComponent) }]
})
export class ImageComponent extends ControllerContainerComponent{

    @Input() public src: string;
    @Input() public alt: string;
    @Input() public width: string = "";
    @Input() public height: string = "";
    
    constructor(eRef: ElementRef, controller: ControllerInputService){
        super(eRef, controller)
    }

    
    public init(cont: Container){
        this.src = cont.data.src;
        this.alt = cont.data.alt;
    }

    
    onInput(event: ControllerInputEvent){};
}