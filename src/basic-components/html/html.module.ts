import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HTMLComponent } from "./html.component";


@NgModule({
    imports: [CommonModule],
    declarations: [
        HTMLComponent
    ],
    providers: [

    ],
    exports: [
        HTMLComponent
    ],
    entryComponents: [
        HTMLComponent
    ]
})
export class HTMLModule{}