import { Component, Input, ElementRef, HostBinding } from "@angular/core";
import { ControllerContainerComponent } from "../../app/components/controller-container/controller-container.component";
import { Container } from "../../app/providers/panel.service";
import { ControllerInputEvent, ControllerInputService } from "../../app/providers/controller-input.service";


@Component({
    selector: "c-html",
    template: ``
})
export class HTMLComponent extends ControllerContainerComponent{

    @HostBinding("innerHTML")
    public html: string = "";
    
    constructor(eRef: ElementRef, controller: ControllerInputService){
        super(eRef, controller)
    }

    
    public init(cont: Container){
        this.html = cont.data.html;

        for(let key of Object.keys(cont.data)){
            if(key == "html")
                continue;
            this.html = this.html.replace("$[" + key, cont.data[key]);
        }  
    }

    
    onInput(event: ControllerInputEvent){};
}