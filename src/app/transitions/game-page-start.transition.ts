import { trigger, animate, style, group, query, transition, keyframes, state } from '@angular/animations';

export const gamePageStart = 

trigger('gamePageStart', [
    
    // TODO: Hide nav bar on application start


    transition("notStarted => started", [
        group([
            query(".content", [
                animate("100ms ease-in", style({
                    opacity: "0",
                    transform: "scale(1.3)"
                }))
            ]),
            query(".background", [
                style({
                    "z-index": "10"
                }),
                animate("700ms ease-out", style({
                    opacity: "1",
                    transform: "scale(1.1)",
                    filter: "blur(0px)"
                }))
            ]),
            query(".background .background-filter", [
                animate("300ms ease-in", style({
                    opacity: "0"
                }))
            ])
        ])
        
    ])

])