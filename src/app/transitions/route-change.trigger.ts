import { trigger, animate, style, group, query, transition } from '@angular/animations';
import { pageFade } from './page.transition';
// import { transitionGamePageToGame } from './game.transition';


export const triggerRouteChange =

    trigger('triggerRouteChange', [
        // transitionGamePageToGame,
        pageFade,
    ]);

