import { trigger, animate, style, group, query, transition, keyframes } from '@angular/animations';

export const pageFade = 

transition('* <=> *', [
            
    // query(':enter, :leave', style({ position: 'absolute', width:'100%' }), { optional: true }),
    group([

        query(':enter', [
            style({  opacity: 0, transform: "scale(1.05)" }),
            animate(
                '0.3s ease-in-out', 
                keyframes([
                    style({ opacity: 0, transform: "scale(1.05)", offset: 0.4}),
                    style({ opacity: 1, transform: "scale(1)", offset: 1})
                ])
            ) 
        ], { optional: true }),

        query(':leave', [
            style({  opacity: 1, transform: "scale(1)" }),
            animate(
                '0.3s ease-in-out',
                keyframes([
                    style({ opacity: 1, transform: "scale(1)", offset: 0}),
                    style({ opacity: 0, transform: "scale(1.01)", offset: 1})
                ])
            ) 
        ] , { optional: true }),
    ])
])