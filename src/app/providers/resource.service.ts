import { Injectable, Type } from "@angular/core";
import { ipcRenderer } from "electron";
import { ConfigService } from "./config.service";
import { query } from "@angular/core/src/render3/query";
import { DomSanitizer, SafeStyle, SafeUrl, SafeValue } from "@angular/platform-browser";
import { replaceAll } from "../utils/string.utils";


@Injectable({
    providedIn: "root"
})
export class ResourceService{

    private dataPath: string = "";

    constructor(private config: ConfigService, private sanitizer: DomSanitizer){
        config.get<string>("dataPath").then(path => {this.dataPath = replaceAll(path, "\\", "/"); console.log("DataPath:", this.dataPath)});
    }

    // TODO: Description
    // Returns a image path
    public getImage(fileName: string): Promise<string>{
        return new Promise<string>((resolve, reject) => {
            var img = new Image();
            img.onload = () => {
                resolve(this.dataPath + "/images/" + fileName);
            }
            img.onerror = () => {
                reject(new Error("Image could not be loaded"));
            }
            this.sanitizer.bypassSecurityTrustUrl(this.dataPath + "/images/" + fileName)
            img.src = this.dataPath + "/images/" + fileName;
            console.log("Loading image", this.dataPath + "/images/" + fileName)
        })
    }

    // TODO: Description
    // Returns a sanitized image
    public async getSanitizedImage(fileName: string, type: SafeType = SafeType.STYLE): Promise<SafeValue>{
        var imgPath = await this.getImage(fileName);

        switch(type){
            case SafeType.URL: return this.sanitizer.bypassSecurityTrustUrl(imgPath);
            case SafeType.STYLE: return this.sanitizer.bypassSecurityTrustStyle(`url(${imgPath})`);
        }
    }

    

}

export enum SafeType{
    HTML,
    URL,
    STYLE,
    RESOURCE
}

