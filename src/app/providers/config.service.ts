import { Injectable } from "@angular/core";
import { ipcRenderer } from "electron";


@Injectable({
    providedIn: "root"
})
export class ConfigService{

    constructor(){
    }

    public get<T>(property: string): Promise<T>{
        return new Promise<T>((resolve, reject) => {
            ipcRenderer.on("config", (sender, data) => {
                resolve(<T> data);
            });
            ipcRenderer.send("config", property);
        });
    }

}