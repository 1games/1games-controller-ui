import { Injectable, Type, NgModuleRef } from '@angular/core';
import { query } from '@angular/core/src/render3/query';
import { Component } from '@angular/compiler/src/core';
import { ControllerContainerComponent } from '../components/controller-container/controller-container.component';
import { PanelComponent } from '../components/panel/panel.component';
import { ipcRenderer } from 'electron';
import { AppModule } from '../app.module';

@Injectable({
  providedIn: 'root'
})
export class PanelService {

  rootPanel: Container = null;

  constructor(private module: NgModuleRef<AppModule>) {
    console.log("Module", this.module);
    // DEBUG: TEsting
   }


  // TODO: Return panel class
  public getRawPanels(): Promise<Array<any>>{
    return new Promise((resolve, reject) => {
      ipcRenderer.on("panel structure", (event, data) => {
        console.log(data);
        resolve(data);
      });
      ipcRenderer.send("panel structure");
    });
  }


  public parseRootPanel(panels: any[]): Container{

    var parsedPanels: Array<Container> = [];

    var rootPanel = new Container(false, null, panels);

    for(let panel of panels){
      parsedPanels.push(this.parsePanel(panel, rootPanel));
    }

    rootPanel.children = parsedPanels;

    this.rootPanel = rootPanel;
    return rootPanel;
  }

  test: number = 0;

  private parsePanel(rawContainer: any, parent: Container): Container{


    var container = new Container(rawContainer.vertical, parent, [], rawContainer.small);
    container.data = rawContainer.data;
    container.type = rawContainer.type ? rawContainer.type : container.type;

    /**
     *  TODO: Assign real component type to string litteral
     */

    if(rawContainer.type == "PanelComponent"){
      container.type = PanelComponent;
    }else if(rawContainer.type == "ControllerContainerComponent"){
      container.type = ControllerContainerComponent
    }

    var children: Array<Container> = []
    if(rawContainer.children){

      for(let child of rawContainer.children){
      
        var panelChild = this.parsePanel(child, container);
        children.push(panelChild);
      }
      
    }
    container.children = children;

    return container;
  }
}

export class Container{
  public type: Type<any> = ControllerContainerComponent;
  private _data: any;
  get data(): any{return this._data}
  set data(value: any){
    this._data = value;
    if(this.data){
      this.execute = this.data.execute;
    }
  }
  public execute: Execute;
  constructor(public vertical = true, public parent: Container, public children: Array<Container> = [], public small: boolean = false){}
}

export interface Execute{
  type: string,

  id?: string, // Replace with enum
  // TODO: Marketplace application id
}
// export class Panel extends Container{
//   constructor(
//     public id: number = 0,
//     public parent: Container,
//     public name: string,
//     public backgroundImg: string,
//     public small: boolean = false
//   ){
//     super(false, parent, [], small)
//   }
// }

// export class IPanel{
//   public id: number = 0
//   public name: string
//   public backgroundImg: string
// }

/*
  { vertical: true },

*/
