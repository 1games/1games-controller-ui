import { Injectable } from "@angular/core";
import { ipcRenderer } from "electron";


@Injectable({
    providedIn: "root"
})
export class GameDetailsService{

    constructor(){

    }



    getData(id: string): Promise<any>{
        return new Promise<any>((resolve, reject) => {

            ipcRenderer.on("game details", (sender, data) => {
                resolve(data);
            });
            ipcRenderer.send("game details", {name: "The Crew", id: id});

        });
    }

}