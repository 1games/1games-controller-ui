import { Injectable, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { ipcRenderer } from "electron";
import { ControllerInputWrapperComponent } from '../components/controller-input-wrapper/controller-input-wrapper.component';


export const controllerInput: Subject<ControllerInputEvent> = new Subject<ControllerInputEvent>();


@Injectable({
  providedIn: 'root'
})
export class ControllerInputService {

  private _currentControllerInputWrapper: ControllerInputWrapperComponent;

  private initialized: boolean = false;

  constructor() { 
    if(!this.initialized)
      this.initListeners();
  }

  get controllerInput(): Subject<ControllerInputEvent>{
    return controllerInput;
  }


  private initListeners(){
    

    ipcRenderer.on("controller input", (ipcEvent, event) => {
      var inputEvent = new ControllerInputEvent(event.controller, event.key, event.value);
    
      var sub = controllerInput.subscribe(data => {
        if(this.currentControllerInputWrapper){
          this.currentControllerInputWrapper.onInput(data);
        }

        sub.unsubscribe();
      })
      
      controllerInput.next(inputEvent);

      

    });


    this.initialized = true;
  }


  set currentControllerInputWrapper(value: ControllerInputWrapperComponent){
    this._currentControllerInputWrapper = value;
    this.currentControllerInputWrapper.notifyUpdate();
  }
  get currentControllerInputWrapper(): ControllerInputWrapperComponent{
    return this._currentControllerInputWrapper;
  }
  
  
}




export class ControllerInputEvent{

  constructor(
      public controller: number = 0,
      public key: InputKey = null,
      public value: number = 0){

  }

  public get direction(): InputDirection{
    if(this.key == InputKey.UP || (this.key == InputKey.AXIS1_Y && this.value > 0.6)){
      return InputDirection.UP
    }else if(this.key == InputKey.DOWN || (this.key == InputKey.AXIS1_Y && this.value < -0.6)){
      return InputDirection.DOWN
    }else if(this.key == InputKey.RIGHT || (this.key == InputKey.AXIS1_X && this.value > 0.6)){
      return InputDirection.RIGHT
    }else if(this.key == InputKey.LEFT || (this.key == InputKey.AXIS1_X && this.value < -0.6)){
      return InputDirection.LEFT
    }
    return InputDirection.NONE;
  }
}

export interface Inputable{
  onInput(event: ControllerInputEvent)
}

export enum InputDirection{
  UP = "UP",
  DOWN = "DOWN",
  LEFT = "LEFT",
  RIGHT = "RIGHT",
  NONE = "NONE"
}

export enum InputKey{
  AXIS1_X = "AXIS1_X",
  AXIS1_Y = "AXIS1_Y",

  AXIS2_X = "AXIS2_X",
  AXIS2_Y = "AXIS2_Y",

  AXIS1_PUSH = "AXIS1_PUSH",
  AXIS2_PUSH = "AXIS2_PUSH",

  LEFT = "LEFT",
  RIGHT = "RIGHT",
  UP = "UP",
  DOWN = "DOWN",

  BUTTON_A = "BUTTON_A",
  BUTTON_B = "BUTTON_B",
  BUTTON_X = "BUTTON_X",
  BUTTON_Y = "BUTTON_Y",

  BACK = "BACK",
  START = "START",

  BUMPER_RIGHT = "BUMPER_RIGHT",
  BUMPER_LEFT = "BUMPER_LEFT",

  TRIGGER_RIGHT = "TRIGGER_RIGHT",
  TRIGGER_LEFT = "TRIGGER_LEFT",

  HOME = "HOME",
}