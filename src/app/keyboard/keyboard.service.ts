import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { KeyboardComponent } from "./keyboard.component";


@Injectable({
    providedIn: "root"
})
export class Keyboard{

    private trigger: Subject<boolean>;
    private keyboard: KeyboardComponent;

    public keyboardReady: Subject<void> = new Subject();

    public get(opt: KeyboardOptions = {}): Promise<string>{
        return new Promise<string>((resolve, reject) => {

            if(opt.initialValue){
                this.keyboard.value = opt.initialValue;
            }

            if(!this.trigger){
                reject(new Error("No trigger set up yet!"));
            }

            this.trigger.next(true);
            this.keyboard.visible = true;
            
            var sub = this.keyboardReady.subscribe(() => {
                sub.unsubscribe();
                this.trigger.next(false);
                this.keyboard.visible = false;
                
                resolve(this.keyboard.value);
            })



        });
    }



    public setup(trigger: Subject<boolean>, keyboard: KeyboardComponent){
        this.trigger = trigger;
        this.keyboard = keyboard;
    }
}

export interface KeyboardOptions{
    initialValue?: string
}