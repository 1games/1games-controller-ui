import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ControllerInputWrapperComponent } from '../components/controller-input-wrapper/controller-input-wrapper.component';
import { ControllerInputService, InputKey } from '../providers/controller-input.service';
import { Keyboard } from './keyboard.service';
import { ButtonComponent } from '../../basic-components/button/button.component';

@Component({
  selector: 'keyboard',
  templateUrl: './keyboard.component.html',
  styleUrls: ['./keyboard.component.less']
})
export class KeyboardComponent implements OnInit{

  @ViewChild(ControllerInputWrapperComponent)
  public inputWrapper: ControllerInputWrapperComponent;

  public visible: boolean = false;

  public value: string = "";
  public caps: boolean = false;

  constructor(private controller: ControllerInputService, private keyboard: Keyboard) { }

  ngOnInit() {

    this.controller.controllerInput.subscribe(event => {


      if(this.visible && event.value == 0){

        if(event.key == InputKey.BUTTON_A){
          var value = (<ButtonComponent>this.inputWrapper.currentPanel).eRef.nativeElement.innerText;
          this.value += value;
        }

        if(event.key == InputKey.BUTTON_B){
          this.keyboard.keyboardReady.next();
        }

        if(event.key == InputKey.BUTTON_X){
          this.value = this.value.substring(0, this.value.length - 1);
        }

        if(event.key == InputKey.BUTTON_Y){
          this.value += " ";          
        }

      }

      if(this.visible && event.key == InputKey.TRIGGER_LEFT){
        this.caps = event.value >= 0.6;
      }

    });
    
    
  }

}
