import { HomeComponent } from './components/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingsMainComponent } from './components/settings-main/settings-main.component';
import { GameDetailsComponent } from './components/game-details/game-details.component';
import { SplashscreenComponent } from './components/splashscreen/splashscreen.component';
import { SettingsComponent } from './components/settings/settings.component';

const routes: Routes = [
    { path: "", component: SplashscreenComponent, data: { state: "splashscreen"} },
    { path: 'home', component: HomeComponent, data: { state: "home"} },
    { path: 'settings/:page', component: SettingsComponent, data: { state: "settings"}},
    { path: 'game-details/:id', component: GameDetailsComponent, data: { state: "game-details"}}
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule { }
