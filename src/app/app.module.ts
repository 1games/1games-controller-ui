import 'zone.js/dist/zone-mix';
import 'reflect-metadata';
import '../polyfills';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { ElectronService } from './providers/electron.service';

import { WebviewDirective } from './directives/webview.directive';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { PanelComponent } from './components/panel/panel.component';
import { ControllerContainerComponent } from './components/controller-container/controller-container.component';
import { ControllerInputWrapperComponent } from './components/controller-input-wrapper/controller-input-wrapper.component';
import { SettingsMainComponent } from './components/settings-main/settings-main.component';
import { GameDetailsComponent } from './components/game-details/game-details.component';
import { PanelService } from './providers/panel.service';
import { ImageModule } from '../basic-components/image/image.module';
import { TextModule } from '../basic-components/text/text.module';
import { HTMLModule } from '../basic-components/html/html.module';
import { ButtonModule } from '../basic-components/button/button.module';
import { GameDetailsService } from './providers/game-details.service';
import { HTMLComponent } from '../basic-components/html/html.component';
import { TextComponent } from '../basic-components/text/text.component';
import { SplashscreenComponent } from './components/splashscreen/splashscreen.component';
import { InputModule } from '../basic-components/input/input.module';
import { KeyboardComponent } from './keyboard/keyboard.component';
import { Keyboard } from './keyboard/keyboard.service';
import { SettingsComponent } from './components/settings/settings.component';
import { SettingsAccountsComponent } from './components/settings-accounts/settings-accounts.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    WebviewDirective,
    PanelComponent,
    ControllerContainerComponent,
    ControllerInputWrapperComponent,
    SettingsMainComponent,
    GameDetailsComponent,
    SplashscreenComponent,
    KeyboardComponent,
    SettingsComponent,
    SettingsAccountsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    BrowserAnimationsModule,
    ImageModule,
    TextModule,
    HTMLModule,
    ButtonModule,
    InputModule
  ],
  providers: [ElectronService, PanelService, GameDetailsService, Keyboard],
  bootstrap: [AppComponent],
  entryComponents: [ControllerContainerComponent, PanelComponent, SettingsMainComponent, SettingsAccountsComponent]
})
export class AppModule { }
