import { Component, OnInit, OnDestroy, ViewChild, AfterContentInit, AfterViewInit, AfterViewChecked } from '@angular/core';
import { ControllerInputService, InputKey } from '../../providers/controller-input.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ControllerInputWrapperComponent } from '../controller-input-wrapper/controller-input-wrapper.component';

@Component({
  selector: 'og-settings-main',
  templateUrl: './settings-main.component.html',
  styleUrls: ['./settings-main.component.less']
})
export class SettingsMainComponent implements OnDestroy, AfterViewInit {

  @ViewChild("inputWrapper")
  inputWrapper: ControllerInputWrapperComponent

  controllerSub: Subscription;

  constructor(private controller: ControllerInputService, private router: Router) {
  }

  ngAfterViewInit() {
    // this.controller.currentControllerInputWrapper = this.inputWrapper;
  }


  ngOnDestroy(){
    if(this.controllerSub) this.controllerSub.unsubscribe();
  }

}
