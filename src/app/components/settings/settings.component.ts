import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { ControllerInputWrapperComponent } from '../controller-input-wrapper/controller-input-wrapper.component';
import { Subscription } from 'rxjs';
import { ControllerInputService, InputKey } from '../../providers/controller-input.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SettingsMainComponent } from '../settings-main/settings-main.component';
import { SettingsAccountsComponent } from '../settings-accounts/settings-accounts.component';

@Component({
  selector: 'og-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.less']
})
export class SettingsComponent implements OnDestroy, AfterViewInit {


  @ViewChild("settingsHost", {read: ViewContainerRef})
  settingsHost: ViewContainerRef;

  controllerSub: Subscription;

  public pages = ["main", "accounts"];

  public page: string = "main"

  constructor(private controller: ControllerInputService, private router: Router, private activatedRoute: ActivatedRoute, private cfr: ComponentFactoryResolver) {
    
  }

  ngAfterViewInit() {

    this.activatedRoute.params.subscribe((params: Params) => {
     
      this.page = params["page"];

      var component;
      if(this.page == "main") component = SettingsMainComponent
      else if(this.page == "accounts") component = SettingsAccountsComponent;

      var factory = this.cfr.resolveComponentFactory(component);

      if(this.settingsHost.clear){
        this.settingsHost.clear();
      }

      var instance = this.settingsHost.createComponent(factory).instance;

      console.log("Instance", instance);
    })    

    this.controllerSub = this.controller.controllerInput.subscribe(event => {
      if(event.value == 0){

        if(event.key == InputKey.BUMPER_LEFT){
          var index = this.pages.indexOf(this.page);  
          if(index > 0){
            this.router.navigateByUrl("settings/" + this.pages[index - 1]);
          }
        }

        if(event.key == InputKey.BUMPER_RIGHT){
          var index = this.pages.indexOf(this.page);  
          if(index < this.pages.length - 1){
            this.router.navigateByUrl("settings/" + this.pages[index + 1]);
          }
        }

      }
    })

  }


  ngOnDestroy(){
    if(this.controllerSub) this.controllerSub.unsubscribe();
  }

}