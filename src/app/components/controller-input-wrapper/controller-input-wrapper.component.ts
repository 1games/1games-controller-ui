import { Component, OnInit, HostListener, ViewChildren, QueryList, AfterViewInit, ContentChildren, AfterContentInit, HostBinding, Input, ElementRef, ComponentFactoryResolver, ViewContainerRef, ViewChild, forwardRef} from '@angular/core';

import { ControllerContainerComponent } from '../controller-container/controller-container.component';
import { PanelComponent } from '../panel/panel.component';
import { InputDirection, ControllerInputEvent, Inputable, ControllerInputService } from '../../providers/controller-input.service';
import { PanelService, Container } from '../../providers/panel.service';
import { query } from '@angular/animations';
import { Subject, Observable } from 'rxjs';




@Component({
  selector: 'controller-input-wrapper',
  templateUrl: './controller-input-wrapper.component.html',
  styleUrls: ['./controller-input-wrapper.component.less'],
  providers: [{provide: ControllerContainerComponent, useExisting: ControllerInputWrapperComponent}]
})
export class ControllerInputWrapperComponent extends ControllerContainerComponent implements OnInit, AfterContentInit {


  @ViewChild("host", { read: ViewContainerRef })
  childHost: ViewContainerRef;

  _rootPanel: Container;
  get rootPanel(): Container{
    return this._rootPanel;
  }
  @Input("rootPanel")
  set rootPanel(value: Container){
    this._rootPanel = value;
    console.log(value)
    this.initRootPanel();
  }
  
  @Input()
  fromContent: boolean = false;

  public panelsCreated: boolean = false;
  currentPanel: ControllerContainerComponent;

  private _initialized: Subject<void> = new Subject<void>();
  get initialized(): Observable<void>{
    return this._initialized.asObservable();
  } 


  constructor(eRef: ElementRef,
      public componentFactoryResolver: ComponentFactoryResolver, controller: ControllerInputService) {
    super(eRef, controller)
    this.selected = true;
    this.vertical = false;
   }

  ngOnInit() {

  }  

  setParentContainer(child: ControllerContainerComponent, parent: ControllerContainerComponent, selected: boolean = false){
    child.parent = parent;
    if(selected && this.selected){
      child.selected = true;
      this.currentPanel = child;
    }
    if(child.children.length > 0){
      for(let childChild of child.children){
        this.setParentContainer(childChild, child)
      }
    }
  }
  
  ngAfterContentInit(){
    super.ngAfterContentInit();


       


  } 

  notifyUpdate(){
    if(this.rootPanel == null || !this.rootPanel || this.fromContent){
      this.initContentPanel();
    }else{
      this.initRootPanel();
    } 
  }

  initContentPanel(){

    console.log("Children", this.children);
    var selected = true;

    for(let child of this.children){
      this.setParentContainer(child, this, selected);
      selected = false;
    }
    
    this.selectFirst(this);
    
    this._initialized.next();
  }

  initRootPanel(){
    if(this.panelsCreated)return;
    var select = true;
    var children = [];
    for(let child of this.rootPanel.children){
      var panel = this.initPanels(child, this, select)
      children.push(panel)
      select = false;
    }

    this.addChildren(children);
    
    this.panelsCreated = true;
    this._initialized.next();
  }

  initPanels(container: Container, host: ControllerContainerComponent, selected: boolean = false): ControllerContainerComponent{
    var panel = this.createPanel(container, host);

    if(selected && host.selected){
      panel.selected = true;

      if(!container.children || container.children.length == 0){
        this.currentPanel = panel;
      }

    }

    var children: Array<ControllerContainerComponent> = [];

    if(container.children){

      var select = true;
      for(let child of container.children){
        var p = this.initPanels(child, panel, select);
        children.push(p);
        select = false;
      }
      panel.addChildren(children);
    }


    return panel;
    

  }


  createPanel(container: Container, parent: ControllerContainerComponent): ControllerContainerComponent{

    let componentFactory = this.componentFactoryResolver.resolveComponentFactory(container.type);

    // host.clear();

    let componentRef = parent.childHost.createComponent(componentFactory);
    let component = <ControllerContainerComponent>componentRef.instance;

    component.parent = parent;

    component.init(container);

    if(component instanceof PanelComponent){
      let panel = <PanelComponent> component;

      panel.size = container.parent.children.length;
      panel.small = parent.small;
      panel.name = container.data.name;
      panel.backgroundImg = container.data.backgroundImg;
      panel.execute = container.execute;
    }

    if(!(component instanceof ControllerContainerComponent)){
      for(let key of Object.keys(component)){
        console.log(key);
        component[key] = container[key];
      }  
  }

      
    return component;
  }


  public move(direction: InputDirection){

    // console.log(this.currentPanel);

    this.moveContainer(direction, this.currentPanel.parent);

    this.findCurrentPanel(this, direction);

    this.focus(this.currentPanel);

    // console.log(this.currentPanel);
    

  }

  // FIXME: When moving out of a container the previous selected panel is still selected! 


  public moveContainer(direction: InputDirection, parent: ControllerContainerComponent, changedContainer: boolean = false){

    if(parent == undefined){
      return;
    }

    if(direction == InputDirection.UP || direction == InputDirection.DOWN){
      // console.log("Direction is vertical")
      if(parent.vertical){

        var success = false;

        if(direction == InputDirection.UP){
          success = parent.previous();
          // console.log("Going up: " + success)
        }else{
          
          success = parent.next();
          // console.log("Going down: " + success)
        }

        if(!success || parent.children.length > 0){
          this.moveContainer(direction, parent.parent, true);
          // this.unselectAll(parent.parent);
        }


      }else{
        this.moveContainer(direction, parent.parent)
      }
    }else if(direction == InputDirection.LEFT || direction == InputDirection.RIGHT){
      if(!parent.vertical){
        // console.log("Parent is not vertical", parent);

        if(changedContainer){
          
          // return;
        }

        var success = false;

        if(direction == InputDirection.LEFT){
          success = parent.previous();
          // console.log("Going left: " + success, parent)
        }else{
          
          success = parent.next();
          // console.log("Going right: " + success, parent)
        }

        if(!success){
          this.moveContainer(direction, parent.parent, true);
          // this.unselectAll(parent.parent);
        }


      }else{
        // console.log("Parent is vertical, moving up", parent);
        this.moveContainer(direction, parent.parent, true)
      }
    }

  }

  public focus(panel: ControllerContainerComponent ){
    // console.log("Focus"); 
    panel.eRef.nativeElement.scrollIntoView({
      behavior: "smooth" 
    });
  }
  

  public findCurrentPanel(parent: ControllerContainerComponent, direction: InputDirection): boolean{

    var success = false;

    for(let child of parent.children){
      if(child.selected){
        if(child.children.length > 0){
          return this.findCurrentPanel(child, direction);
        }else{
          this.currentPanel = child;

          return true;
        }
      }
    }

    this.unselectAll(this.currentPanel.parent);
    if(direction == InputDirection.DOWN || direction == InputDirection.RIGHT){
      this.selectFirst(parent);
    }else if(direction == InputDirection.LEFT || direction == InputDirection.UP){
      this.selectLast(parent);
    }
    console.log("Nothing found");

    return false;

  }

  public unselectAll(parent: ControllerContainerComponent){
    for(let child of parent.children){
      child.selected = false;
      if(child.children.length > 0){
        this.unselectAll(child);
      }
    }
  }

  public selectFirst(parent: ControllerContainerComponent){
    parent.selected = true;
    if(parent.children.length > 0){
      parent.select(0);
      this.selectFirst(parent.children[0]);
    }else{
      this.currentPanel = parent
      parent.select(0);
    }
  }

  public selectLast(parent: ControllerContainerComponent){
    parent.selected = true;
    if(parent.children.length > 0){
      parent.select(parent.children.length - 1);
      this.selectLast(parent.children[parent.children.length - 1]);
    }else{
      this.currentPanel = parent
      parent.select(parent.children.length - 1);
    }
    }

  

  onInput(event: ControllerInputEvent){
    if(event.direction && Math.abs(event.value) >= 0.6){
      this.move(event.direction);
    }else{
      console.log("onInput" in this.currentPanel, this.currentPanel)
      if("onInput" in this.currentPanel){
        (<Inputable>this.currentPanel).onInput(event);
      }
    }
  }


}

