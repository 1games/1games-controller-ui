import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControllerInputWrapperComponent } from './controller-input-wrapper.component';

describe('ControllerInputWrapperComponent', () => {
  let component: ControllerInputWrapperComponent;
  let fixture: ComponentFixture<ControllerInputWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControllerInputWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControllerInputWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
