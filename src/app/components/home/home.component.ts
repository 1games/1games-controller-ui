import { Component, OnInit, ViewChild } from '@angular/core';
import { PanelService } from '../../providers/panel.service';
import { ControllerInputService } from '../../providers/controller-input.service';
import { ControllerInputWrapperComponent } from '../controller-input-wrapper/controller-input-wrapper.component';

@Component({
  selector: 'og-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {

  @ViewChild(ControllerInputWrapperComponent)
  private controllerInputWrapper: ControllerInputWrapperComponent;

  constructor(public panel: PanelService, public controllerInput: ControllerInputService) {

  }

  ngOnInit() {
    this.controllerInput.currentControllerInputWrapper = this.controllerInputWrapper;
    this.panel.getRawPanels().then(data => {
      this.controllerInputWrapper.rootPanel = this.panel.parseRootPanel(data);
    });
  }


  
}
