import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ipcRenderer } from 'electron';

@Component({
  selector: 'og-splashscreen',
  templateUrl: './splashscreen.component.html',
  styleUrls: ['./splashscreen.component.less']
})
export class SplashscreenComponent implements OnInit {

  constructor(
    private router: Router
  ) { 

  }

  ngOnInit() {

    console.log("Loading data...");
    
    
    // ipcRenderer.once("bootstrap finished", (event, request) => {
      this.router.navigateByUrl("home")
    // });



  }

}
