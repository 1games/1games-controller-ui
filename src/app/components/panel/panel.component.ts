import { Component, OnInit, ElementRef, HostBinding, Input, forwardRef } from '@angular/core';
import { ControllerContainerComponent } from '../controller-container/controller-container.component';
// import { Panel, IPanel } from '../../providers/panel.service';
import { ControllerInputService, InputKey, ControllerInputEvent } from '../../providers/controller-input.service';
import { Router } from '@angular/router';
import { Execute } from '../../providers/panel.service';


@Component({
  selector: 'panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.less'],
  providers: [{provide: ControllerContainerComponent, useExisting: forwardRef(() => PanelComponent) }]
})
export class PanelComponent extends ControllerContainerComponent implements OnInit {

  @HostBinding("attr.size")
  size: number = 2;

  public name: string = ""
  public backgroundImg: string = ""
  public id: number = 0;
  public execute: Execute;

  @HostBinding("class.focus")
  public focus: boolean = false;

  constructor(eRef: ElementRef, controller: ControllerInputService, private router: Router,) {
    super(eRef, controller);
  }

  ngOnInit() {

    // this.controller.controllerInput.subscribe(input => {

    //   if(this.selected && input.key == InputKey.BUTTON_A){
    //     if(input.value == 0){
    //       this.focus = true;
    //     }else{
    //       this.focus = false;
    //       this.router.navigateByUrl("/game-details/" + this.id);
    //     }
    //   }
    // });

  }
  
  onInput(event: ControllerInputEvent){
    if(event.value == 0){
      if(event.key == InputKey.BUTTON_A){
        this.router.navigateByUrl("game-details/" + this.execute.id);
      }
    }
  };

}
