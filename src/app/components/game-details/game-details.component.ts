import { Component, OnInit, ViewChild, HostBinding } from '@angular/core';
import { ControllerInputService, InputKey } from '../../providers/controller-input.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PanelService, Container } from '../../providers/panel.service';
import { PanelComponent } from '../panel/panel.component';
import { ImageComponent } from '../../../basic-components/image/image.component';
import { HTMLComponent } from '../../../basic-components/html/html.component';
import { ControllerInputWrapperComponent } from '../controller-input-wrapper/controller-input-wrapper.component';
import { GameDetailsService } from '../../providers/game-details.service';
import { DomSanitizer, SafeStyle, SafeUrl } from '@angular/platform-browser';
import { ResourceService, SafeType } from '../../providers/resource.service';
import { ipcRenderer } from 'electron';
import { gamePageStart } from '../../transitions/game-page-start.transition';

@Component({
  selector: 'og-game-details',
  templateUrl: './game-details.component.html',
  styleUrls: ['./game-details.component.less'],
  animations: [
    gamePageStart
  ]
})
export class GameDetailsComponent implements OnInit {

  @ViewChild(ControllerInputWrapperComponent)
  private ciw: ControllerInputWrapperComponent;

  // panel: Panel = new Panel(0, null, "", ");
  private squareImg: any = "";
  private backgroundImg: any = "";

  public screenshots = []

  private id: string = "";

  @HostBinding("@gamePageStart")
  private gameState = "notStarted";

  constructor(
    public controller: ControllerInputService,
    public router: Router,
    private route: ActivatedRoute,
    public panels: PanelService,
    public game: GameDetailsService,
    public sanitizer: DomSanitizer,
    public resources: ResourceService) { }

  ngOnInit() {
    this.controller.controllerInput.subscribe(input => {
      if(input.key == InputKey.BUTTON_B){
        this.router.navigateByUrl("");
      }

      if(input.value == 0 && input.key == InputKey.BUTTON_A){
        if(this.ciw.currentPanel.eRef.nativeElement.classList.contains("play")){
          // ipcRenderer.send("game start", {id: this.id})
          this.gameState = "started";
          setTimeout(() => {
            this.gameState = "idle"
          }, 698);
        }
      }
    }); 


    this.route.params.subscribe((params: Params) => {
      this.id = params["id"];
      this.game.getData(this.id).then(data => {

        console.log("Data", data);

        this.controller.currentControllerInputWrapper = this.ciw;


        this.resources.getSanitizedImage(data.imgSquare, SafeType.URL).then((path) => {
          console.log(path);
          this.squareImg = path;
        });

        this.resources.getSanitizedImage(data.imgDefault).then((path) => {
          console.log(path);
          this.backgroundImg = path;
        });

        


        var count = 0;
        for(let screenshot of data.screenshots){

          this.resources.getSanitizedImage(screenshot, SafeType.URL).then(img => {
            this.screenshots.push(img);
            
            if(++count == data.screenshots.length){

              console.log(count, data.screenshots.length);
              
              // this.controllerInputWrapper.initContentPanel();
            }
          });
          
        }


      });
      // TODO: Handle exception
    });
    
    // console.log(this.game)
    // this.game.getData().then(data => {
    //   console.log(Object.keys(data));

    //   var img = new Image()
    //   img.onload = () => {
    //     this.backgroundImg = this.sanitizer.bypassSecurityTrustStyle(data.imgDefault)
    //   }
    //   img.src = data.imgDefault
      
    //   // this.backgroundImg = this.sanitizer.bypassSecurityTrustUrl();
    // });
  }

}
