import { OnInit, ElementRef, ViewContainerRef, ViewChild, HostBinding, Component, ContentChildren, AfterContentInit, QueryList, forwardRef, Input, Output, EventEmitter } from '@angular/core';
import { Container } from '../../providers/panel.service';
import { ControllerInputWrapperComponent } from '../controller-input-wrapper/controller-input-wrapper.component';
import { findNewEntries } from '../../utils/array.utils';
import { ControllerInputService } from '../../providers/controller-input.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'controller-container',
  templateUrl: './controller-container.component.html',
  styleUrls: ['./controller-container.component.less']
})
export class ControllerContainerComponent implements OnInit, AfterContentInit {

  _selected: boolean = false;
  set selected (selected: boolean){
    this._selected = selected;
    this.onSelected.emit();
  }
  @HostBinding("class.selected")
  get selected () : boolean{
    return this._selected;
  }

  @HostBinding("attr.cSelected")
  private _childSelected: number = 0;

  get childSelected(): number{
    return this._childSelected
  }

  @HostBinding("class.vertical")
  public vertical: boolean = true;
  @Input("vertical")
  set _vertical(value: string){
    this.vertical = value == "false" ? false : true
  }

  @Input()
  public parent: ControllerContainerComponent;
  
  @ContentChildren(ControllerContainerComponent)
  private childrenQueryList: QueryList<ControllerContainerComponent>;
  
  private childComponents: ControllerContainerComponent[] = [];

  @HostBinding("attr.small")
  small: boolean = false;

  @Output()
  public onSelected: EventEmitter<void> = new EventEmitter();
  
  @ViewChild("host", { read: ViewContainerRef })
  public childHost: ViewContainerRef;

  @Input()
  public title;


  constructor(public eRef: ElementRef, private controller: ControllerInputService) { 

  }

  ngOnInit() {
  }

  public init(cont: Container){
    this.vertical = cont.vertical;
    this.small = cont.small;

    if(cont.data ){
      
      this.title = cont.data.title;
      if(cont.data.style){

        for(let prop of Object.keys(cont.data.style)){
          this.eRef.nativeElement.style[prop] = cont.data.style[prop];
        }
      }
    }
  }

  
  
  ngAfterContentInit(){
    this.childrenQueryList.notifyOnChanges();
    this.childrenQueryList.changes.subscribe(changes => {
      console.log("Changes", changes);

      this.childComponents = this.childrenQueryList.toArray().filter(x => x !== this);

      this.controller.currentControllerInputWrapper.notifyUpdate();
      // var curParent = this.parent;
      // while(curParent != undefined && curParent.parent != undefined){
      //   console.log("Panel", curParent);
      //   curParent = curParent.parent;
      // }

      // if(curParent != null){
      //   (<ControllerInputWrapperComponent>curParent).notifyUpdate();
      // }



    })
    this.childComponents = this.childComponents.concat(this.childrenQueryList.toArray().filter(x => x !== this))
    // console.log("Container: ", this.eRef, this.childrenQueryList, this.childComponents);
  }

  
  get children(): Array<ControllerContainerComponent>{
    return this.childComponents;
  }

  addChildren(...items: ConcatArray<ControllerContainerComponent>[]){
    for(let i of items){
      this.childComponents = this.childComponents.concat(i);
    }
  }



  //Returns true if successful
  next(): boolean{
    console.log("Child selected: " + this.childSelected + ": " + this.children.length);
    if(this.childSelected < this.children.length - 1){
      this._childSelected++;
      this.updateChildren();
      return true;
    }
    this.updateChildren();
    return false;
  }

  // Returns true if successful
  previous(): boolean{
    if(this.childSelected > 0){
      this._childSelected--;
      this.updateChildren();
      return true;
    }
    this.updateChildren();
    return false;
  }

  select(index: number){
    this._childSelected = Math.max(Math.min(index, this.children.length - 1), 0);
  }


  updateChildren(){
    var counter = 0;
    for(let child of this.children){
      child.selected = counter == this.childSelected && this.selected
      counter++;
    }
  }

  // abstract onInput(event: ControllerInputEvent);
  
}
