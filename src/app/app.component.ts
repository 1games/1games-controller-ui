import { Component, HostListener, OnInit, Type, ViewChild, ViewContainerRef, AfterViewInit } from '@angular/core';
import { ElectronService } from './providers/electron.service';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from '../environments/environment';
import { InputDirection, ControllerInputEvent, InputKey, ControllerInputService } from './providers/controller-input.service';
import { triggerRouteChange } from './transitions/route-change.trigger';
import { HomeComponent } from './components/home/home.component';
import { SettingsMainComponent } from './components/settings-main/settings-main.component';
import { ActivatedRoute, Router, ActivationEnd } from '@angular/router';
import { ControllerInputWrapperComponent } from './components/controller-input-wrapper/controller-input-wrapper.component';
import { KeyboardComponent } from './keyboard/keyboard.component';
import { Keyboard } from './keyboard/keyboard.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  animations: [ triggerRouteChange ]
})
export class AppComponent implements OnInit, AfterViewInit{

  time: string = "";

  pages: Array<any> = [
    {
      pageComponent: HomeComponent,
      route: "",
      title: "startseite",
      icon: "home"
    },
    {
      pageComponent: HomeComponent,
      route: "friends",
      title: "freunde",
      icon: "people"
    },
    {
      pageComponent: HomeComponent,
      route: "achievements",
      title: "errungenschaften",
      image: "prize.png"
    }
  ]
  activePage: string = "";
  activePageIndex: number = 0;

  hideNav: boolean = false;

  
  tmpInputWrapper: ControllerInputWrapperComponent;

  @ViewChild("profileMenuInputWrapper")
  profileMenuInputWrapper: ControllerInputWrapperComponent;
  profileMenuVisible: boolean = false;

  @ViewChild("keyboardHost")
  keyboardHost: KeyboardComponent;
  keyboardVisible: boolean = false;



  constructor(
    public electronService: ElectronService,
    private translate: TranslateService,
    public controllerInput: ControllerInputService,
    public router: Router,
    public keyboard: Keyboard) {

    translate.setDefaultLang('en');
    console.log('AppConfig', AppConfig);

    if (electronService.isElectron()) {
      console.log('Mode electron');
      // console.log('Electron ipcRenderer', electronService.ipcRenderer);
      console.log('NodeJS childProcess', electronService.childProcess);
    } else {
      console.log('Mode web');
    }


    
    
  }

  ngOnInit(){

    this.router.navigateByUrl("");
    this.startClockTimer();
    this.controllerInput.controllerInput.subscribe(input => {
      if(input.value == 0){

        if(input.key == InputKey.BUMPER_LEFT){
          this.activePageIndex--;
        }else if(input.key == InputKey.BUMPER_RIGHT){
          this.activePageIndex++;
        }else if(input.key == InputKey.START){

          if(this.profileMenuVisible){
            this.profileMenuVisible = false;
            if(this.tmpInputWrapper){
              this.controllerInput.currentControllerInputWrapper = this.tmpInputWrapper;
              this.tmpInputWrapper = null;
            }
          }else{

            this.tmpInputWrapper = this.controllerInput.currentControllerInputWrapper;
            this.controllerInput.currentControllerInputWrapper = this.profileMenuInputWrapper;

            this.profileMenuVisible = true;
          }


          return;
        }
        
        if(this.activePageIndex < 0) this.activePageIndex = 0;
        if(this.activePageIndex >= this.pages.length) this.activePageIndex = this.pages.length - 1;
        
        this.activePage = this.pages[this.activePageIndex].route;
      }
    });

    this.router.events.subscribe(event => {
      if(event instanceof ActivationEnd){
        this.hideNav = event.snapshot.component != this.pages[this.activePageIndex].pageComponent
      }
    })

  
  }

  ngAfterViewInit(){
    var keyboardTrigger = new Subject<boolean>();
    keyboardTrigger.subscribe((visible) => {

      if(visible){
        this.tmpInputWrapper = this.controllerInput.currentControllerInputWrapper;
        this.controllerInput.currentControllerInputWrapper = this.keyboardHost.inputWrapper;
      }else{
        this.controllerInput.currentControllerInputWrapper = this.tmpInputWrapper;
      }

      this.keyboardVisible = visible;
    });

    this.keyboard.setup(keyboardTrigger, this.keyboardHost);
  }

  prepareRouteTransition(outlet) {
    return outlet.activatedRouteData.state;
  }

  onMenuClicked(event: ControllerInputEvent, item: string){
    if(event.value == 0 && event.key == InputKey.BUTTON_A){
      console.log("Menu click", event)
      this.profileMenuVisible = false;
      if(item == "settings") this.router.navigateByUrl("settings/main");
      if(item == "home") this.router.navigateByUrl("")
    }
  }

  private startClockTimer(){
    setInterval(() => {
      var cur = new Date();
      this.time = (cur.getDate() < 10 ? "0" + cur.getDate() : cur.getDate())  + "." + (cur.getMonth()+1 < 10 ? "0" + (cur.getMonth()+1) : cur.getMonth()+1)  + ". " + (cur.getHours() < 10 ? "0" + cur.getHours() : cur.getHours()) + ":" + (cur.getMinutes() < 10 ? "0" + cur.getMinutes() : cur.getMinutes()) + " Uhr " 
    }, 1000)
  }

  
}
