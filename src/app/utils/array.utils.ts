export function findNewEntries<T = any>(old: Array<T>, newArray: Array<T>): Array<T>{
    var changes = new Array<T>();
    for(let n of newArray){
        var isOld = false;
        for(let o of old){
            if(o == n){
                isOld = true;
            }
        }
        if(!isOld){
            changes.push(n);
        }
    }
    return changes;
}