export function replaceAll(string: string, search: string, replacement: string): string{
    return string.replace(new RegExp(escapeRegExp(search), 'g'), replacement);
}
export function escapeRegExp(str: string): string {
    return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
}