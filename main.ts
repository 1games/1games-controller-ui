import { app, BrowserWindow, screen, globalShortcut } from 'electron';
import * as path from 'path';
import * as url from 'url';

import { ControllerInputHandler} from "./lib/controller/ControllerInput";
import { GameDetails } from "./lib/games/GameDetails";
import { PanelManager } from "./lib/panels/PanelManager";
import { Config } from './lib/config/Config';

const Steam = require("steam");

let win: BrowserWindow, serve;
const args = process.argv.slice(1);
serve = args.some(val => val === '--serve');

require('source-map-support').install();


export const ROOT_PATH = __dirname;

// const cache = new Cache();

// TODO: May replace this with system specific path
var config: Config;


let controllerInput: ControllerInputHandler;

function createWindow() {

  const electronScreen = screen;
  const size = electronScreen.getPrimaryDisplay().workAreaSize;

  // Create the browser window.
  win = new BrowserWindow({
    x: 20,
    y: 0,
    width: size.width,
    height: size.height,
    fullscreenable: true,
    webPreferences:{
      webSecurity: !serve
    }
    // frame: false
  });

  if (serve) {
    require('electron-reload')(__dirname, {
      electron: require(`${__dirname}/node_modules/electron`)
    });
    win.loadURL('http://localhost:4200');
  } else {
    win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
  }

  win.webContents.openDevTools();

  win.setMenu(null);
  win.setFullScreenable(true);

  globalShortcut.register("F11", () => {
    win.setFullScreen(!win.isFullScreen());
  })

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store window
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });

  config = new Config(win, __dirname + "/config/main.json");
  controllerInput = new ControllerInputHandler(win);

  var gameDetails = new GameDetails(win, config);
  var panelManager = new PanelManager(config);


  var steamClient = new Steam.SteamClient();
  var steamUser = new Steam.SteamUser(steamClient);
  var steamFriends = new Steam.SteamFriends(steamClient);

  setTimeout(() => {

    win.webContents.send("bootstrap finished");
  }, 5000);
}

try {

  // This method will be called when Electron has finished
  // initialization and is ready to create browser windows.
  // Some APIs can only be used after this event occurs.
  app.on('ready', createWindow);

  // Quit when all windows are closed.
  app.on('window-all-closed', () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
      app.quit();
    }
  });

  app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
      createWindow();
    }
  });

} catch (e) {
  // Catch Error
  // throw e;
}
